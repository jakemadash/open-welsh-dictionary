package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Word;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class TestWelshDictionary {

    @Test
    public void testRunWelshDictionary() {
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getWords("bod");
        Assertions.assertEquals(1, words.size());
    }

    @Test
    public void testHoffiWelshDictionary() {
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getWords("cael");

        Assertions.assertEquals(1, words.size());
    }


    @Test
    public void testLemmaWithMutatedForm(){
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getLemmas("fyddwn");
        Assertions.assertEquals(1, words.size());
    }

    @Test
    public void testWordWithExistingLemma(){
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getLemmas("byddwn");
        Assertions.assertEquals(1, words.size());
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new WelshDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }


}

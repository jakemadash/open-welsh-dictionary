package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.List;


/**
 * Represents a Welsh noun
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class WelshNoun extends WelshWord {

    private static final String PLURAL = "plural";

    private final List<String> plural;

    public WelshNoun(JSONObject obj) {
        super(obj, WordType.noun);
        plural = Json.getStringListOrNull(obj, PLURAL);
    }

    @Override
    protected JSONObject getInflections() {
        JSONObject obj = new JSONObject();
        addIfExist(obj, PLURAL, plural);
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(plural != null)
            list.addAll(plural);
        return list;
    }
}

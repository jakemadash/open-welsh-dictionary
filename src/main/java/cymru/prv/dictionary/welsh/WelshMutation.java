package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;


/**
 * Takes the word and stores all the possible
 * mutations of the word.
 *
 * @author Preben Vangberg
 * @since 1.0.2
 */
public class WelshMutation implements JsonSerializable {

    private final String radical;
    private String soft;
    private String nasal;
    private String aspirate;


    /**
     * Takes the word and create and generates
     * all the possible mutations of the word.
     *
     * @param word the word to mutate
     */
    public WelshMutation(String word){
        radical = word;

        if(word.matches("p[^h].*"))
            apply(word.substring(1), "b", "mh", "ph");
        else if(word.matches("t[^h].*"))
            apply(word.substring(1), "d", "nh", "th");
        else if(word.matches("c[^h].*"))
            apply(word.substring(1), "g", "ngh", "ch");
        else if(word.startsWith("b"))
            apply(word.substring(1), "f", "m", null);
        else if(word.matches("d[^d].*"))
            apply(word.substring(1), "dd", "n", null);
        else if(word.startsWith("g"))
            apply(word.substring(1), "", "ng", null);
        else if(word.startsWith("ll"))
            apply(word.substring(2), "l", null, null);
        else if(word.startsWith("rh"))
            apply(word.substring(2), "r", null, null);
        else if(word.startsWith("m"))
            apply(word.substring(1), "f", null, null);
    }

    private void apply(String word, String soft, String nasal, String aspirate){
        this.soft = soft + word;
        if(nasal != null)
            this.nasal = nasal + word;
        if(aspirate != null)
            this.aspirate = aspirate + word;
    }


    /**
     * Returns the radical version of
     * the word
     *
     * @return the radical version
     */
    public String getRadical() {
        return radical;
    }


    /**
     * Returns the soft mutated version
     * if it exists. Otherwise null
     *
     * @return the soft mutated version
     */
    public String getSoft() {
        return soft;
    }


    /**
     * Returns the nasal mutated version
     * if it exists. Otherwise null
     *
     * @return the nasal mutated version
     */
    public String getNasal() {
        return nasal;
    }


    /**
     * Returns the aspirate mutated version
     * if it exists. Otherwise null
     *
     * @return the aspirate mutated version
     */
    public String getAspirate() {
        return aspirate;
    }


    /**
     * Creates a list of all the versions
     * of the word that is not null and
     * returns it
     *
     * @return all the possible forms of the word
     */
    public List<String> getVersions(){
        List<String> versions = new LinkedList<>();
        versions.add(radical);
        if(soft != null)
            versions.add(soft);
        if(nasal != null)
            versions.add(nasal);
        if(aspirate != null)
            versions.add(aspirate);
        return versions;
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("init", radical);
        obj.put("soft", soft != null ? soft : "");
        obj.put("nasal", nasal != null ? nasal : "");
        obj.put("aspirate", aspirate != null ? aspirate : "");
        return obj;
    }
}

package cymru.prv.dictionary.welsh;

import java.util.regex.Pattern;

/**
 * A helper class to count
 * syllables in a Welsh word
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class Syllable {

    private Syllable(){}

    private static final Pattern pattern = Pattern.compile("(ae|ai|au|aw|ei|eu|ew|iw|wy|yw|a|e|i|o|u|w|y)");

    public static long countSyllables(String word){
        return pattern.matcher(word).results().count();
    }

}

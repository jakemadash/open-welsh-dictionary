package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;


/**
 * The basic structure for all Welsh tenses
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public abstract class WelshVerbTense extends Conjugation {

    protected final String stem;

    public WelshVerbTense(WelshVerb verb, JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", verb.getStem());
    }

    private static final Map<Character, String> makeLong = Map.of(
            'a', "â",
            'o', "ô",
            'e', "ee"
    );

    protected String apply(String suffix){
        boolean endsWithFirstLetterOfSuffix = stem.endsWith("" + suffix.charAt(0));
        if(endsWithFirstLetterOfSuffix && makeLong.containsKey(suffix.charAt(0)) && suffix.length() > 1)
            return stem.replaceFirst( suffix.charAt(0) + "$", "" + makeLong.get(suffix.charAt(0))) + suffix.substring(1);
        if(endsWithFirstLetterOfSuffix)
                return stem + suffix.substring(1);
         return stem + suffix;
    }

    @Override
    protected abstract List<String> getDefaultSingularFirst();

    @Override
    protected abstract List<String> getDefaultSingularSecond();

    @Override
    protected abstract List<String> getDefaultSingularThird();

    @Override
    protected abstract List<String> getDefaultPluralFirst();

    @Override
    protected abstract List<String> getDefaultPluralSecond();

    @Override
    protected abstract List<String> getDefaultPluralThird();
}
